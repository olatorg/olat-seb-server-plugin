# OLAT SEB Server Plugin

[![License](https://img.shields.io/badge/License-Apache--2.0-blue)](https://www.apache.org/licenses/LICENSE-2.0)
[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](CODE_OF_CONDUCT.md)
[![OpenOlat](https://img.shields.io/badge/OpenOlat-19.1--SNAPSHOT-1aa6be)](https://gitlab.com/olatorg/openolat-starter/)
[![Spring Boot](https://img.shields.io/badge/Spring_Boot-3.4.1-6bb536)](https://docs.spring.io/spring-boot/3.4/index.html)

SEB Server Plugin for [OLAT](https://www.olat.org/).

## Usage

1. Add this repository to the `repositories` section of your project's `pom.xml`.

   ```xml
   <repositories>
     <repository>
       <id>olatorg-repo</id>
       <url>https://gitlab.com/api/v4/groups/olatorg/-/packages/maven</url>
     </repository>
   </repositories>
   ```

2. Add this dependency to the `dependencies` section.

   ```xml
   <dependency>
     <groupId>org.olat.plugin</groupId>
     <artifactId>olat-seb-server-plugin</artifactId>
     <version>${olat-seb-server-plugin.version}</version>
   </dependency>
   ```

3. Make sure the REST API and the managed assessment modes are enabled in OLAT. E.g in
   `application.yml`:

   ```yml
   openolat:
     properties:
       assessment.mode.managed: true
       restapi.enable: true
   ```

## Development

1. Clone the source code.

   ```shell
   git clone git@gitlab.com:olatorg/olat-seb-server-plugin.git
   cd olat-seb-server-plugin
   ```

2. Build project.

   ```shell
   ./mvnw verify
   ```

## License

This project is Open Source software released under
the [Apache 2.0 license](https://www.apache.org/licenses/LICENSE-2.0).
