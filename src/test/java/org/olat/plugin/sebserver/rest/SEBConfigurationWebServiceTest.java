/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.plugin.sebserver.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.jakarta.rs.json.JacksonJsonProvider;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.util.List;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.jaxrs.lifecycle.SingletonResourceProvider;
import org.apache.cxf.transport.local.LocalConduit;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.olat.course.assessment.AssessmentModeManager;
import org.olat.course.assessment.AssessmentModule;
import org.olat.course.assessment.model.AssessmentModeImpl;
import org.olat.plugin.sebserver.rest.SEBConfigurationWebService.SEBConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;

/**
 * @author Christian Schweizer
 * @since 1.0
 */
@SpringJUnitWebConfig(locations = "classpath:test-context.xml")
@ExtendWith(MockitoExtension.class)
class SEBConfigurationWebServiceTest {

  private static final String ENDPOINT_ADDRESS = "local://repo/assessmentmodes/1/seb";
  private Server server;
  private WebClient client;

  @SuppressWarnings("unused")
  @MockBean
  private AssessmentModule assessmentModule;

  @MockBean AssessmentModeManager assessmentModeManager;

  @BeforeEach
  void startServer() {
    when(assessmentModeManager.getAssessmentModeById(1L))
        .then(
            invocation -> {
              AssessmentModeImpl assessmentMode = new AssessmentModeImpl();
              assessmentMode.setKey(1L);
              return assessmentMode;
            });

    final JAXRSServerFactoryBean factory = new JAXRSServerFactoryBean();

    factory.setAddress(ENDPOINT_ADDRESS);
    factory.setProviders(List.of(new JacksonJsonProvider()));
    factory.setResourceClasses(SEBConfigurationWebService.class);
    factory.setResourceProvider(
        SEBConfigurationWebService.class,
        new SingletonResourceProvider(new SEBConfigurationWebService(assessmentModeManager), true));
    server = factory.create();
  }

  @AfterEach
  void destroyServer() {
    server.stop();
    server.destroy();
  }

  @BeforeEach
  void createClient() {
    client = WebClient.create(ENDPOINT_ADDRESS, List.of(new JacksonJsonProvider()));
    WebClient.getConfig(client).getRequestContext().put(LocalConduit.DIRECT_DISPATCH, Boolean.TRUE);
  }

  @AfterEach
  void closeClient() {
    client.close();
  }

  @Test
  void getSEBConfiguration() {
    SEBConfiguration response =
        client
            .path("/repo/assessmentmodes/1/seb")
            .accept(MediaType.APPLICATION_JSON)
            .get(SEBConfiguration.class);
    assertThat(response).isNotNull();
    assertThat(response.getKey()).isEqualTo(1L);
  }

  @Test
  void postSEBConfiguration() {
    SEBConfiguration configuration = new SEBConfiguration();
    configuration.setKey(1L);
    configuration.setConfigKeys(List.of("12345678"));
    configuration.setBrowserExamKeys(List.of("12345678"));
    configuration.setQuitLink("https://example.org/");
    configuration.setQuitSecret("quit");
    SEBConfiguration response =
        client
            .path("/repo/assessmentmodes/1/seb")
            .type(MediaType.APPLICATION_JSON)
            .post(configuration, SEBConfiguration.class);
    assertThat(response).isNotNull();
    assertThat(response.getKey()).isEqualTo(1L);
  }

  @Test
  void deleteSEBConfiguration() {
    try (Response response =
        client.path("/repo/assessmentmodes/1/seb").type(MediaType.APPLICATION_JSON).delete()) {
      assertThat(response.getStatus()).isEqualTo(200);
      assertThat(response.hasEntity()).isTrue();
      SEBConfiguration configuration = response.readEntity(SEBConfiguration.class);
      assertThat(configuration).isNotNull();
      assertThat(configuration.getKey()).isEqualTo(1L);
    }
  }
}
