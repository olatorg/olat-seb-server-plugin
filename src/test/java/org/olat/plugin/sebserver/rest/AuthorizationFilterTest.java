/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.plugin.sebserver.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.olat.core.gui.UserRequest;
import org.olat.core.id.Roles;
import org.olat.core.util.UserSession;
import org.olat.restapi.security.RestSecurityHelper;

/**
 * @author Christian Schweizer
 * @since 1.0
 */
@ExtendWith(MockitoExtension.class)
class AuthorizationFilterTest {

  @Test
  void not_authenticated_request(@Mock ContainerRequestContext requestContext) {
    AuthorizationFilter filter = new AuthorizationFilter();
    filter.filter(requestContext);
    ArgumentCaptor<Response> argument = ArgumentCaptor.forClass(Response.class);
    verify(requestContext, times(1)).abortWith(argument.capture());
    assertThat(argument.getValue().getStatus()).isEqualTo(Status.UNAUTHORIZED.getStatusCode());
  }

  @Test
  void authenticated_request_guest_roles(@Mock ContainerRequestContext requestContext) {
    when(requestContext.hasProperty(RestSecurityHelper.SEC_USER_REQUEST)).thenReturn(true);
    AuthorizationFilter filter = new AuthorizationFilter();
    filter.filter(requestContext);
    ArgumentCaptor<Response> argument = ArgumentCaptor.forClass(Response.class);
    verify(requestContext, times(1)).abortWith(argument.capture());
    assertThat(argument.getValue().getStatus()).isEqualTo(Status.FORBIDDEN.getStatusCode());
  }

  @Test
  void authenticated_request_administrator_roles(
      @Mock ContainerRequestContext requestContext,
      @Mock UserRequest userRequest,
      @Mock UserSession userSession) {
    when(requestContext.hasProperty(RestSecurityHelper.SEC_USER_REQUEST)).thenReturn(true);
    when(requestContext.getProperty(RestSecurityHelper.SEC_USER_REQUEST)).thenReturn(userRequest);
    when(userRequest.getUserSession()).thenReturn(userSession);
    when(userSession.getRoles()).thenReturn(Roles.administratorRoles());
    AuthorizationFilter filter = new AuthorizationFilter();
    filter.filter(requestContext);
    verify(requestContext, never()).abortWith(any());
  }
}
