/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.plugin.sebserver.app;

import java.util.List;
import org.olat.autoconfigure.openolat.DefaultUserProvider;
import org.olat.user.DefaultUser;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * @author Christian Schweizer
 * @since 1.0
 */
@SpringBootApplication
public class Application {

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

  @Bean
  public DefaultUserProvider defaultUserProvider() {
    return () -> {
      DefaultUser defaultUser = new DefaultUser("sebserver-admin");
      defaultUser.setFirstName("SEB Server");
      defaultUser.setLastName("Administrator");
      defaultUser.setEmail("sebserver-admin@example.org");
      defaultUser.setPassword("sebserver-admin-pw");
      defaultUser.setAdmin(true);
      return List.of(defaultUser);
    };
  }
}
