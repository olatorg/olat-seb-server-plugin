/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.plugin.sebserver.autoconfigure;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.mock;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.olat.core.commons.persistence.DB;
import org.olat.core.id.context.HistoryManager;
import org.olat.core.util.coordinate.CoordinatorManager;
import org.olat.core.util.session.UserSessionManager;
import org.olat.core.util.session.UserSessionModule;
import org.olat.course.assessment.AssessmentModeManager;
import org.olat.course.assessment.AssessmentModule;
import org.springframework.boot.autoconfigure.AutoConfigurations;
import org.springframework.boot.test.context.runner.ApplicationContextRunner;
import org.springframework.context.annotation.Bean;

/**
 * @author Christian Schweizer
 * @since 1.0
 */
@ExtendWith(MockitoExtension.class)
class SEBServerAutoConfigurationTest {

  private final ApplicationContextRunner contextRunner =
      new ApplicationContextRunner()
          .withPropertyValues(
              "assessment.mode.managed=false",
              "session.timeout=0",
              "session.timeout.authenticated=0")
          .withConfiguration(AutoConfigurations.of(SEBServerAutoConfiguration.class))
          .withUserConfiguration(TestConfiguration.class);

  @Test
  void contextLoads() {
    contextRunner.run(
        context -> {
          assertThat(context).hasNotFailed();
          assertThat(context).hasSingleBean(SEBServerProperties.class);
        });
  }

  @Test
  void contextLoads_whenDisabled() {
    contextRunner
        .withPropertyValues("olat.seb-server.enabled=false")
        .run(
            context -> {
              assertThat(context).hasNotFailed();
              assertThat(context).doesNotHaveBean(SEBServerProperties.class);
            });
  }

  static class TestConfiguration {

    @Bean
    public AssessmentModeManager assessmentModeManager() {
      return mock(AssessmentModeManager.class);
    }

    @Bean
    public AssessmentModule assessmentModule() {
      return mock(AssessmentModule.class);
    }

    @Bean
    public CoordinatorManager coordinatorManager() {
      return mock(CoordinatorManager.class);
    }

    @Bean
    public DB db() {
      return mock(DB.class);
    }

    @Bean
    public HistoryManager historyManager() {
      return mock(HistoryManager.class);
    }

    @Bean
    public UserSessionManager userSessionManager() {
      return mock(UserSessionManager.class);
    }

    @Bean
    public UserSessionModule userSessionModule() {
      return mock(UserSessionModule.class);
    }
  }
}
