/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.plugin.sebserver.check;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

import jakarta.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.junit.jupiter.MockitoExtension;
import org.olat.commons.coordinate.cluster.ClusterCoordinator;
import org.olat.core.CoreSpringFactory;
import org.olat.core.gui.UserRequest;
import org.olat.core.util.WebappHelper;
import org.olat.core.util.coordinate.Coordinator;
import org.olat.core.util.coordinate.CoordinatorManager;
import org.olat.core.util.coordinate.CoordinatorManagerImpl;
import org.olat.core.util.event.EventBus;
import org.olat.core.util.i18n.I18nManager;
import org.olat.core.util.i18n.I18nModule;
import org.olat.core.util.i18n.LocalStringsCustomizer;
import org.olat.course.assessment.model.TransientAssessmentMode;
import org.olat.plugin.sebserver.autoconfigure.SEBServerProperties;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * @author Christian Schweizer
 * @since 1.1
 */
@ExtendWith({MockitoExtension.class, SpringExtension.class})
@ContextConfiguration(classes = RestrictSEBVersionCheckTest.TestConfiguration.class)
@WebAppConfiguration
class RestrictSEBVersionCheckTest {

  private SEBServerProperties properties;

  @BeforeEach
  void setUp() {
    properties = new SEBServerProperties();
    properties.getRestriction().setWindows(List.of("1.0.0", "1.1.0", "1.2.0"));
    properties.getRestriction().setMacos(List.of("1.0.0", "1.1.0", "1.2.0"));
    properties.getRestriction().setIos(List.of("1.0.0", "1.1.0", "1.2.0"));
  }

  @ParameterizedTest
  @ValueSource(
      strings = {
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.3 SEB/1.0.0", // Windows
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.3 SEB/1.0.0", // macOS
        "Mozilla/5.0 (iPhone; CPU iPhone OS 17_5_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.5 Mobile/15E148 Safari/604. SEB/1.0.0", // iOS
        "Mozilla/5.0 (iPad; CPU OS 14_6_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) FxiOS/129.0 Mobile/15E148 Safari/605.1.15 SEB/1.0.0" // iPadOS
      })
  void check_withCorrectSEBVersion(String userAgent) {
    UserRequest userRequest = mock(UserRequest.class);
    TransientAssessmentMode assessmentMode = mock(TransientAssessmentMode.class);
    when(userRequest.getHttpReq()).thenReturn(createMockRequest(userAgent));
    StringBuilder errors = new StringBuilder();
    RestrictSEBVersionCheck check = new RestrictSEBVersionCheck(properties);
    boolean result = check.check(userRequest, assessmentMode, errors);
    assertThat(result).isTrue();
    assertThat(errors).isEmpty();
  }

  @ParameterizedTest
  @ValueSource(
      strings = {
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.3 SEB/2.0.0", // Windows
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.3 SEB/2.0.0", // macOS
        "Mozilla/5.0 (iPhone; CPU iPhone OS 17_5_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.5 Mobile/15E148 Safari/604. SEB/2.0.0", // iOS
        "Mozilla/5.0 (iPad; CPU OS 14_6_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) FxiOS/129.0 Mobile/15E148 Safari/605.1.15 SEB/2.0.0" // iPadOS
      })
  void check_withWrongSEBVersion(String userAgent) {
    UserRequest userRequest = mock(UserRequest.class);
    TransientAssessmentMode assessmentMode = mock(TransientAssessmentMode.class);
    when(userRequest.getHttpReq()).thenReturn(createMockRequest(userAgent));
    when(userRequest.getLocale()).thenReturn(Locale.getDefault());
    StringBuilder errors = new StringBuilder();
    RestrictSEBVersionCheck check = new RestrictSEBVersionCheck(properties);
    boolean result = check.check(userRequest, assessmentMode, errors);
    assertThat(result).isFalse();
    assertThat(errors).isNotEmpty();
  }

  @ParameterizedTest
  @ValueSource(
      strings = {
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.3 SEB/1.0.0", // Windows
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.3 SEB/1.0.0", // macOS
        "Mozilla/5.0 (iPhone; CPU iPhone OS 17_5_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.5 Mobile/15E148 Safari/604. SEB/1.0.0", // iOS
        "Mozilla/5.0 (iPad; CPU OS 14_6_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) FxiOS/129.0 Mobile/15E148 Safari/605.1.15 SEB/1.0.0" // iPadOS
      })
  void check_withNoRestriction(String userAgent) {
    UserRequest userRequest = mock(UserRequest.class);
    TransientAssessmentMode assessmentMode = mock(TransientAssessmentMode.class);
    lenient().when(userRequest.getHttpReq()).thenReturn(createMockRequest(userAgent));
    lenient().when(userRequest.getLocale()).thenReturn(Locale.getDefault());
    RestrictSEBVersionCheck check = new RestrictSEBVersionCheck(new SEBServerProperties());
    StringBuilder errors = new StringBuilder();
    boolean result = check.check(userRequest, assessmentMode, errors);
    assertThat(result).isTrue();
    assertThat(errors).isEmpty();
  }

  private HttpServletRequest createMockRequest(String userAgent) {
    MockHttpServletRequest request = new MockHttpServletRequest();
    request.addHeader("User-Agent", userAgent);
    return request;
  }

  static class TestConfiguration {

    @Bean
    public I18nManager i18nManager(
        I18nModule i18nModule, ObjectProvider<LocalStringsCustomizer> customizers) {
      return new I18nManager(i18nModule, customizers);
    }

    @Bean
    public I18nModule i18nModule(CoordinatorManager coordinatorManager, WebappHelper webappHelper) {
      return new I18nModule(coordinatorManager, webappHelper);
    }

    @Bean
    public CoordinatorManager coordinatorManager(Coordinator coordinator) {
      CoordinatorManagerImpl coordinatorManager =
          BeanUtils.instantiateClass(CoordinatorManagerImpl.class);
      coordinatorManager.setCoordinator(coordinator);
      return coordinatorManager;
    }

    @Bean
    public Coordinator coordinator() {
      ClusterCoordinator coordinator = new ClusterCoordinator();
      coordinator.setEventBus(mock(EventBus.class));
      return coordinator;
    }

    @Bean
    public CoreSpringFactory coreSpringFactory() {
      return BeanUtils.instantiateClass(CoreSpringFactory.class);
    }

    @Bean
    public WebappHelper webappHelper() {
      WebappHelper webappHelper = new WebappHelper();
      // Makes sure to write output in temp directory
      webappHelper.setUserDataRoot(null);
      return webappHelper;
    }
  }
}
