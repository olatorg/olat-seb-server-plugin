/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.plugin.sebserver.rest;

import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.Provider;
import org.olat.core.gui.UserRequest;
import org.olat.core.id.Roles;
import org.olat.restapi.security.RestSecurityHelper;

/**
 * @author Christian Schweizer
 * @since 1.0
 */
@Provider
@Secured
@SuppressWarnings("unused")
public class AuthorizationFilter implements ContainerRequestFilter {

  @Override
  public void filter(ContainerRequestContext requestContext) {
    if (requestContext.hasProperty(RestSecurityHelper.SEC_USER_REQUEST)) {
      Roles roles;
      UserRequest userRequest =
          (UserRequest) requestContext.getProperty(RestSecurityHelper.SEC_USER_REQUEST);
      if (userRequest == null
          || userRequest.getUserSession() == null
          || userRequest.getUserSession().getRoles() == null) {
        roles = Roles.guestRoles();
      } else {
        roles = userRequest.getUserSession().getRoles();
      }
      validateRoles(roles, requestContext);
    } else {
      requestContext.abortWith(Response.status(Status.UNAUTHORIZED).build());
    }
  }

  private void validateRoles(Roles roles, ContainerRequestContext requestContext) {
    if (!roles.isAdministrator() && !roles.isLearnResourceManager()) {
      requestContext.abortWith(Response.status(Status.FORBIDDEN).build());
    }
  }
}
