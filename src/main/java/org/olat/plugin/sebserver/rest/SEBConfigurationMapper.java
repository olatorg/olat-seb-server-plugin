/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.plugin.sebserver.rest;

import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.olat.course.assessment.AssessmentMode;
import org.olat.plugin.sebserver.rest.SEBConfigurationWebService.SEBConfiguration;

/**
 * @author Christian Schweizer
 * @since 1.0
 */
@Mapper
public interface SEBConfigurationMapper {

  @Mapping(source = "safeExamBrowserKey", target = "browserExamKeys")
  @Mapping(source = "safeExamBrowserConfigPListKey", target = "configKeys")
  @Mapping(source = "safeExamBrowserConfiguration.linkToQuit", target = "quitLink")
  @Mapping(source = "safeExamBrowserConfiguration.passwordToExit", target = "quitSecret")
  SEBConfiguration assessmentModeToSEBConfiguration(AssessmentMode assessmentMode);

  default List<String> mapStringFromList(String value) {
    if (value == null) {
      return List.of();
    }
    return Arrays.asList(StringUtils.split(value, ','));
  }
}
