/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.plugin.sebserver.rest;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import java.util.List;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.mapstruct.factory.Mappers;
import org.olat.course.assessment.AssessmentMode;
import org.olat.course.assessment.AssessmentModeManager;
import org.olat.course.assessment.model.AssessmentModeImpl;
import org.olat.course.assessment.model.SafeExamBrowserConfiguration;

/**
 * @author Christian Schweizer
 * @since 1.0
 */
@Secured
@Path("repo/assessmentmodes/{key}/seb")
@RequiredArgsConstructor
public class SEBConfigurationWebService {

  private final AssessmentModeManager assessmentModeManager;
  private final SEBConfigurationMapper mapper = Mappers.getMapper(SEBConfigurationMapper.class);

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public SEBConfiguration getSEBConfiguration(@PathParam("key") Long key) {
    AssessmentMode assessmentMode = assessmentModeManager.getAssessmentModeById(key);
    return mapper.assessmentModeToSEBConfiguration(assessmentMode);
  }

  @POST
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  public SEBConfiguration postSEBConfiguration(
      @PathParam("key") Long key, SEBConfiguration sebConfiguration) {
    AssessmentMode assessmentMode = assessmentModeManager.getAssessmentModeById(key);
    assessmentMode.setSafeExamBrowser(
        (sebConfiguration.browserExamKeys != null && !sebConfiguration.browserExamKeys.isEmpty())
            || sebConfiguration.configKeys != null && !sebConfiguration.configKeys.isEmpty());
    if (assessmentMode.isSafeExamBrowser()) {
      // Set SafeExamBrowserConfiguration before setting config key! It would be recalculated
      // otherwise.
      SafeExamBrowserConfiguration configuration = new SafeExamBrowserConfiguration();
      configuration.setLinkToQuit(sebConfiguration.getQuitLink());
      configuration.setPasswordToExit(sebConfiguration.quitSecret);
      assessmentMode.setSafeExamBrowserConfiguration(configuration);
      if (assessmentMode instanceof AssessmentModeImpl impl) {
        impl.setSafeExamBrowserConfigPListKey(String.join(",", sebConfiguration.configKeys));
      }
      assessmentMode.setSafeExamBrowserKey(String.join(",", sebConfiguration.browserExamKeys));
      assessmentMode.setManagedFlagsString("safeexambrowser");
      assessmentMode.setSafeExamBrowserConfigDownload(false);
    }
    assessmentModeManager.persist(assessmentMode);
    return mapper.assessmentModeToSEBConfiguration(assessmentMode);
  }

  @DELETE
  @Produces(MediaType.APPLICATION_JSON)
  public SEBConfiguration deleteSEBConfiguration(@PathParam("key") Long key) {
    AssessmentMode assessmentMode = assessmentModeManager.getAssessmentModeById(key);
    assessmentMode.setSafeExamBrowser(false);
    assessmentMode.setSafeExamBrowserConfiguration(null);
    assessmentMode.setManagedFlagsString(null);
    assessmentModeManager.persist(assessmentMode);
    return mapper.assessmentModeToSEBConfiguration(assessmentMode);
  }

  @Getter
  @Setter
  public static class SEBConfiguration {

    private long key;
    private List<String> browserExamKeys;
    private List<String> configKeys;
    private String quitLink;
    private String quitSecret;
  }
}
