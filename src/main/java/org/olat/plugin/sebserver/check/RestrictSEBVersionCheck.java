/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.plugin.sebserver.check;

import java.util.List;
import java.util.StringTokenizer;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.translator.Translator;
import org.olat.core.util.Util;
import org.olat.course.assessment.model.TransientAssessmentMode;
import org.olat.extension.course.assessment.ui.mode.AssessmentModeCheck;
import org.olat.plugin.sebserver.autoconfigure.SEBServerProperties;

/**
 * This class checks whether a user tries to open an assessment with a specific version of Safe Exam
 * Browser (SEB). <br>
 * The corresponding version can be configured in an {@code application.yml} file.
 *
 * @author Christian Schweizer
 * @since 1.1
 */
@Log4j2
@RequiredArgsConstructor
public class RestrictSEBVersionCheck implements AssessmentModeCheck {

  private final SEBServerProperties properties;

  @Override
  public boolean check(
      UserRequest userRequest, TransientAssessmentMode assessmentMode, StringBuilder errors) {
    String currentVersion = getSEBVersion(userRequest);
    OS os = getOS(userRequest);
    if (properties.getRestriction() != null && currentVersion != null && os != null) {
      List<String> versions = null;
      switch (os) {
        case WINDOWS -> versions = properties.getRestriction().getWindows();
        case MACOS -> versions = properties.getRestriction().getMacos();
        case IOS -> versions = properties.getRestriction().getIos();
      }
      if (versions != null && !versions.contains(currentVersion)) {
        Translator translator =
            Util.createPackageTranslator(RestrictSEBVersionCheck.class, userRequest.getLocale());
        log.info(
            "Current SEB version [{}] not found in supported versions {}",
            currentVersion,
            versions);
        errors.append("<h4><i class='o_icon o_icon_warn o_icon-fw'>&nbsp;</i>");
        errors.append(translator.translate("error.seb.version.not.supported"));
        errors.append("</h4>");
        errors.append(
            translator.translate(
                "error.seb.version.not.supported.desc",
                currentVersion,
                String.join(", ", versions)));
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  }

  private String getSEBVersion(UserRequest userRequest) {
    String userAgent = userRequest.getHttpReq().getHeader("User-Agent");
    if (userAgent != null) {
      StringTokenizer stringTokenizer = new StringTokenizer(userAgent);
      while (stringTokenizer.hasMoreTokens()) {
        String currentToken = stringTokenizer.nextToken();
        if (currentToken.startsWith("SEB/") && currentToken.length() > 4) {
          return currentToken.substring(4);
        }
      }
    }
    return null;
  }

  private OS getOS(UserRequest userRequest) {
    String userAgent = userRequest.getHttpReq().getHeader("User-Agent");
    if (userAgent != null) {
      if (userAgent.toLowerCase().contains("win")) {
        return OS.WINDOWS;
      } else if (userAgent.toLowerCase().contains("mac")) {
        return OS.MACOS;
      } else if (userAgent.toLowerCase().contains("ipad")
          || userAgent.toLowerCase().contains("iphone")) {
        return OS.IOS;
      }
    }
    return null;
  }

  enum OS {
    WINDOWS,
    MACOS,
    IOS
  }
}
