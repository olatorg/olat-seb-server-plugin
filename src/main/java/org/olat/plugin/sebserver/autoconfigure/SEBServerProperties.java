/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.plugin.sebserver.autoconfigure;

import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author Christian Schweizer
 * @since 1.0
 */
@ConfigurationProperties(prefix = "olat.seb-server")
@Getter
@Setter
public class SEBServerProperties {

  private boolean enabled;
  private Restriction restriction = new Restriction();

  @Getter
  @Setter
  public static class Restriction {
    private List<String> macos;
    private List<String> windows;
    private List<String> ios;
  }
}
